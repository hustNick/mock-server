const express = require("express");
const morgan = require("morgan");
const app = express();

app.use(morgan("combined"));

app.all("/mfp/api/adapters/customerAdapter/GET_SecurityCheck", (req, res) => {
  res.status(401).send();
  // res.json({
  //   logout: false,
  //   doRiskAssessment: true,
  //   isSuccessful: true,
  //   errors: [],
  // });
});

app.all(
  "/mfp/api/adapters/onlineGuardianAdapter/POST_riskAssessmentOnLogin",
  (req, res) => {
    res.json({
      logout: false,
      statusReason: "No Content",
      responseHeaders: { RqUID: "52d6231d-43e0-4991-9824-014531826830" },
      isSuccessful: true,
      warnings: [],
      errors: [],
      info: [],
      statusCode: 204,
    });
  }
);

app.all("/mfp/api/adapters/customerAdapter/GET_CustomerDataV2", (req, res) => {
  res.json({
    logout: false,
    statusReason: "OK",
    responseHeaders: { RqUID: "e294fb90-db87-403a-aecc-a73a0342b451" },
    isSuccessful: true,
    response: {
      productsResponse: {
        kbaStatus: "Set",
        legalType: "individual",
        mobileNumber: "*** *** **22",
        selfSelectedID: "user",
        lastLoginDate: "20200713",
        lastPasswordChangeDate: "09 Jul 2020",
        isGuardianSetup: true,
        products: {
          product: [
            {
              loanDeferralEligibility: false,
              accountType: "20",
              canMakeInternationalPaymentFrom: true,
              accountOpenDate: "2014-04-25",
              canTransferTo: true,
              uuid: "9595fe9d-71d1-4a18-82f2-c553ff189f08",
              ccProductAccountNumber: "",
              accountKey:
                "Aa4ui4WCEeUFbvW3TcUL8b5rOMieJ2JdxLyzoKWiApjv_WBppZ43zC8NWpT5saKkpc0OedcGRW3V1atAn1W6OQ4XHGxXWWdLkTkEQkaMXIZS25VtpfeRJSkDyzABVdjjw9N2kLWV1dRLAu2UdZmN3iU",
              availableBalance: "$131,098.32",
              isCifAccount: true,
              number: "03 1592 0659734 000",
              balance: "-$7,111.68",
              canTransferFrom: true,
              name: "Choices",
              limit: "$138,210.00",
              canListTransactions: true,
              id: "0",
              canPayFrom: true,
            },
            {
              loanDeferralEligibility: false,
              accountType: "10",
              canMakeInternationalPaymentFrom: true,
              accountOpenDate: "2018-03-13",
              canTransferTo: true,
              uuid: "5dea37db-b59e-4ad1-b7d9-967d332eeff6",
              ccProductAccountNumber: "",
              accountKey:
                "AeNKGpBp5Fb18gyMGi8PqYtBxiYsru8gngLaa8V-eBecBCpe5ZXYNQuUkBuEwQw1qQSnmu3NHJyOxm8n_eHsrPeWbFiMlLGenqrUz5IorppSmOOmCyXn_LWXX14Md0_FlG9ZlCeehRJPsqniyopcuiA",
              availableBalance: "$0.00",
              isCifAccount: true,
              number: "03 1592 0659734 001",
              balance: "$0.00",
              canTransferFrom: true,
              name: "Westpac Everyday",
              nickname: "Felix McGirr Busines",
              canListTransactions: true,
              id: "1",
              canPayFrom: true,
            },
            {
              loanDeferralEligibility: false,
              accountType: "50",
              accountOpenDate: "2014-04-29",
              uuid: "d45cc486-d9be-4164-ad84-4581e85d5166",
              ccProductAccountNumber: "",
              accountKey:
                "AZ3sWAmFz6e-nqx0-mcfDXhL5oEt2hqJrbl7ILwENQABWq9pNzrudJgOPX2ffIz4EH3iJBLREoU2sDmyLYPF32gnVBIjZxAlYe863WA89I3u6Dc0lc9zW5fvbsh8m17oMugSRJN_SD-Sxg7uciB_k_k",
              availableBalance: "$0.00",
              isCifAccount: true,
              number: "03 1592 0659734 094",
              balance: "$0.00",
              name: "Choices",
              limit: "$87,320.00",
              canListTransactions: true,
              id: "2",
              canPayFrom: false,
            },
            {
              isTermDeposit: true,
              loanDeferralEligibility: false,
              accountType: "50",
              accountOpenDate: "2020-04-04",
              uuid: "c8848cde-0360-430e-8b8b-da4d4c910a07",
              ccProductAccountNumber: "",
              accountKey:
                "AeDP8y-y2mm9hdbJzL06qBZi99obLZHeJW26lpp5j29Pwb_wl-ZOocAizIC5N0nn9kX9oX-hVtaFi11w79g6VNIJaua3BXXx0IuD9nsaqEFNKhMMxqRiX7gtgUaT-INckV_laHxoEeaOjHOZ_cbk3I0",
              availableBalance: "$0.00",
              isCifAccount: true,
              number: "03 1592 0659734 081",
              balance: "$730,000.00",
              name: "Term investment",
              id: "3",
              canPayFrom: false,
            },
          ],
        },
        mailReturned: false,
        serverDate: "2020-07-13T12:42:02+1200",
        emailAddress: "test0065503519@westpac.co.nz",
        emailSuspended: false,
        customerId: "334701462",
        selfCertificationObligation: null,
        auiid: "bd1c210086f63a0dffcb618ed75a8f57c8feb3ea",
        status: "success",
      },
    },
    warnings: [],
    errors: [],
    info: [],
    statusCode: 200,
  });
});

app.listen(3000);
